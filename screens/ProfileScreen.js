import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class ProfileScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
           
            <Text>Jonard Berdos Tanilon</Text>
            <Text>19 years old</Text>
            <Text>jonardTanilon98@gmail.com </Text>
            <Text>University of Southeastern Philippines</Text>
            <Text>BS of Information Technology</Text>
            <Text>BSIT-3B / 3rd year</Text>
            <Text>ICLC Vice-Governor 2018</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

const fontSize = StyleSheet.create({
    container: {
      fontSize: 20, 
    },
  });


export default ProfileScreen;
