import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text> Welcome!</Text>
            <Button
              title="Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'cyan',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default HomeScreen;
